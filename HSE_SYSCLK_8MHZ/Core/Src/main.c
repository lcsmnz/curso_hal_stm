
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "main.h"
#include <string.h>

#define FALSE 0
#define TRUE 1

void UART2_Init(void);
void Error_handler(void);
void GPIOA_Init(void);

GPIO_InitTypeDef Button;
UART_HandleTypeDef H_uart2;

int main(void)
{
	RCC_OscInitTypeDef Osc_Init;
	RCC_ClkInitTypeDef Clk_Init;
	uint32_t Clk_value;
	char *msg = "TESTE";

	HAL_Init();
	UART2_Init();
	GPIOA_Init();

	memset(&Osc_Init,0,sizeof(Osc_Init));
	Osc_Init.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	Osc_Init.HSEState = RCC_HSE_ON;

	if(HAL_RCC_OscConfig(&Osc_Init) != HAL_OK)
	{
		Error_handler();
	}

	Clk_Init.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	Clk_Init.SYSCLKSource= RCC_SYSCLKSOURCE_HSE;
	Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV2;
	Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;
	Clk_Init.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&Clk_Init, FLASH_ACR_LATENCY_0WS) != HAL_OK)
	{
		Error_handler();
	}

	__HAL_RCC_HSI_DISABLE();
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
	Clk_value = HAL_RCC_GetSysClockFreq();

	UART2_Init();
	GPIOA_Init();

	while(1)
	{
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == GPIO_PIN_SET){}

			msg = "SYS  clk:";
			HAL_UART_Transmit(&H_uart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
			Clk_value = HAL_RCC_GetSysClockFreq();
			HAL_UART_Transmit(&H_uart2, (uint8_t*)Clk_value, 4, HAL_MAX_DELAY);

			msg = "HCLK clk:";
			HAL_UART_Transmit(&H_uart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
			Clk_value = HAL_RCC_GetHCLKFreq();
			HAL_UART_Transmit(&H_uart2, (uint8_t*)Clk_value, 4, HAL_MAX_DELAY);

			msg = "P1CLK:";
			HAL_UART_Transmit(&H_uart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
			Clk_value = HAL_RCC_GetPCLK1Freq();
			HAL_UART_Transmit(&H_uart2, (uint8_t*)Clk_value, 4, HAL_MAX_DELAY);

			msg = "P2CLK:";
			HAL_UART_Transmit(&H_uart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
			Clk_value = HAL_RCC_GetSysClockFreq();
			HAL_UART_Transmit(&H_uart2, (uint8_t*)Clk_value, 4, HAL_MAX_DELAY);

			break;
	}

	return 0;
}


void UART2_Init(void)
{
	H_uart2.Instance = USART2;
	H_uart2.Init.BaudRate = 115200;
	H_uart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	H_uart2.Init.Mode = UART_MODE_TX_RX;
	H_uart2.Init.Parity = UART_PARITY_NONE;
	H_uart2.Init.StopBits = UART_STOPBITS_1;
	H_uart2.Init.WordLength = UART_WORDLENGTH_8B;
	if( HAL_UART_Init(&H_uart2) != HAL_OK)
	{
		// There is some problem in the initialization
		Error_handler();
	}
}

void GPIOA_Init(void)
{
	Button.Mode = GPIO_MODE_INPUT;
	Button.Pin = GPIO_PIN_0;
	Button.Speed = GPIO_SPEED_FAST;
	Button.Pull = GPIO_NOPULL;

	HAL_GPIO_Init(GPIOA, &Button);
	__HAL_RCC_GPIOA_CLK_ENABLE();

}

void Error_handler(void)
{
	while(1);
}
