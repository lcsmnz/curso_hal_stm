
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "main.h"
#include <string.h>

#define FALSE 0
#define TRUE 1

void SystemClockConfig(void);
void UART2_Init(void);
void Error_handler(void);

UART_HandleTypeDef H_uart2;
char *user_data = "the application is running\r\n";
uint8_t data_buffer[100];
uint8_t rcvd_data, rcpt_cmplt = FALSE;
uint32_t cont=0;


int main(void)
{
	HAL_Init();
	SystemClockConfig();
	UART2_Init();
	uint16_t len_of_data = strlen(user_data);

	HAL_UART_Transmit(&H_uart2, (uint8_t*)user_data, len_of_data, HAL_MAX_DELAY);


	HAL_UART_Receive_IT(&H_uart2, &rcvd_data, 1);

	while(1);

	return 0;
}

void SystemClockConfig(void)
{
}

void UART2_Init(void)
{
	H_uart2.Instance = USART2;
	H_uart2.Init.BaudRate = 115200;
	H_uart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	H_uart2.Init.Mode = UART_MODE_TX_RX;
	H_uart2.Init.Parity = UART_PARITY_NONE;
	H_uart2.Init.StopBits = UART_STOPBITS_1;
	H_uart2.Init.WordLength = UART_WORDLENGTH_8B;
	if( HAL_UART_Init(&H_uart2) != HAL_OK)
	{
		// There is some problem in the initialization
		Error_handler();
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(rcvd_data = '\r')
	{
		//reception completed
		rcpt_cmplt = TRUE;
	}
	data_buffer[cont++] = rcvd_data;

}

void Error_handler(void)
{
	while(1);
}
