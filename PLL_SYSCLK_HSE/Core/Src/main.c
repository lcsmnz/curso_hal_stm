
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "main.h"
#include <string.h>

#define FALSE 0
#define TRUE 1

void UART2_Init(void);
void Error_handler(void);
void GPIOA_Init(void);
void SystemClock_config_HSE(uint8_t Clock_freq);

GPIO_InitTypeDef Button;
UART_HandleTypeDef H_uart2;

int main(void)
{

	HAL_Init();
	UART2_Init();
	GPIOA_Init();
	SystemClock_config_HSE(SYS_CLOCK_FREQ_50MHZ);

	while(1);

	return 0;
}

void SystemClock_config_HSE(uint8_t Clock_freq)
{
	RCC_OscInitTypeDef Osc_Init;
	RCC_ClkInitTypeDef Clk_Init;
	uint32_t F_latency = 0;


	Osc_Init.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	Osc_Init.HSEState = RCC_HSE_BYPASS;
	Osc_Init.PLL.PLLState = RCC_PLL_ON;
	Osc_Init.PLL.PLLSource = RCC_PLLSOURCE_HSE;

	Clk_Init.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	Clk_Init.SYSCLKSource= RCC_SYSCLKSOURCE_PLLCLK;
	Clk_Init.APB2CLKDivider = RCC_HCLK_DIV2;

	switch(Clock_freq)
	{
		case SYS_CLOCK_FREQ_50MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 100;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV2;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_1WS;
			break;
		case SYS_CLOCK_FREQ_84MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 168;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;


			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_2WS;
			break;
		case SYS_CLOCK_FREQ_120MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 240;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV4;

			F_latency = FLASH_ACR_LATENCY_3WS;
			break;
		case SYS_CLOCK_FREQ_168MHZ:

			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 240;
			Osc_Init.PLL.PLLP = 2;
			Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV4;

			F_latency = FLASH_ACR_LATENCY_3WS;

			Osc_Init.PLL.PLLM = 4;
			Osc_Init.PLL.PLLN = 168;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV2;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_3WS;

		break;
		default:
			return;
	}

	if( HAL_RCC_OscConfig(&Osc_Init) != HAL_OK)
	{
		Error_handler();
	}

	if( HAL_RCC_ClockConfig(&Clk_Init, F_latency) != HAL_OK)
	{
		Error_handler();
	}

	//systick configuration

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}

void UART2_Init(void)
{
	H_uart2.Instance = USART2;
	H_uart2.Init.BaudRate = 115200;
	H_uart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	H_uart2.Init.Mode = UART_MODE_TX_RX;
	H_uart2.Init.Parity = UART_PARITY_NONE;
	H_uart2.Init.StopBits = UART_STOPBITS_1;
	H_uart2.Init.WordLength = UART_WORDLENGTH_8B;
	if( HAL_UART_Init(&H_uart2) != HAL_OK)
	{
		// There is some problem in the initialization
		Error_handler();
	}
}

void GPIOA_Init(void)
{
	Button.Mode = GPIO_MODE_INPUT;
	Button.Pin = GPIO_PIN_0;
	Button.Speed = GPIO_SPEED_FAST;
	Button.Pull = GPIO_NOPULL;

	HAL_GPIO_Init(GPIOA, &Button);
	__HAL_RCC_GPIOA_CLK_ENABLE();

}

void Error_handler(void)
{
	while(1);
}
