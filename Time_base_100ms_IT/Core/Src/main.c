
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "main.h"

void Error_handler(void);
void SystemClockConfig(void);
void TIMER6_Init(void);
void GPIO_LED_Init(void);

TIM_HandleTypeDef htimer6;

int main(void)
{

	HAL_Init();
	SystemClockConfig();
	GPIO_LED_Init();
	TIMER6_Init();

	//Start the timer
	HAL_TIM_Base_Start_IT(&htimer6);

	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
	while(1)
	{
	}

	return 0;
}

void SystemClockConfig(void)
{


}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
}

void GPIO_LED_Init(void)
{
	GPIO_InitTypeDef ledgpio;
	__HAL_RCC_GPIOD_CLK_ENABLE();

	//PD14
	ledgpio.Mode = GPIO_MODE_OUTPUT_PP;
	ledgpio.Speed = GPIO_SPEED_FAST;
	ledgpio.Pull = GPIO_NOPULL;

	ledgpio.Pin = GPIO_PIN_12;
	HAL_GPIO_Init(GPIOD, &ledgpio);

	ledgpio.Pin = GPIO_PIN_13;
	HAL_GPIO_Init(GPIOD, &ledgpio);

	ledgpio.Pin = GPIO_PIN_14;
	HAL_GPIO_Init(GPIOD, &ledgpio);

	ledgpio.Pin = GPIO_PIN_15;
	HAL_GPIO_Init(GPIOD, &ledgpio);
}

void TIMER6_Init(void)
{
	htimer6.Instance = TIM6;
	htimer6.Init.Prescaler = 24;
	htimer6.Init.Period = 0xFFFF-1;
	if(HAL_TIM_Base_Init(&htimer6) != HAL_OK)
	{
		Error_handler();
	}
}

void Error_handler(void)
{
	while(1);
}
