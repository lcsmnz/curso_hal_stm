
#include "main.h"

TIM_HandleTypeDef htimer6;


void HAL_MspInit(void)
{
	// Here we will do low level processor specific inits

	//1. Set the piority grouping of the ARM CORTEX MX processor
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	//2. Enable the required system exceptions of the ARM CORTEX MX processor
	SCB->SHCSR |= 0x7 << 16; // USG, memory and Bus fault system exceptions enable.

	//3. Configure the priority for the system exceptions
	HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
	HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
	HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htimer)
{
	//first enable the clock fot timer 6
	__HAL_RCC_TIM6_CLK_ENABLE();

	//Enable IRQ of timer 6
	HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);

	//Set up the priority of IRQn
	HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 15, 0);



}
