
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_gpio.h"
#include "main.h"

void Error_handler(void);
void SystemClock_config_HSI(uint8_t Clock_freq);
void TIMER6_Init(void);
void GPIO_LED_Init(void);

TIM_HandleTypeDef htimer6;

int main(void)
{

	HAL_Init();
	SystemClock_config_HSI(SYS_CLOCK_FREQ_50MHZ);
	GPIO_LED_Init();
	TIMER6_Init();

	//Start the timer
	HAL_TIM_Base_Start_IT(&htimer6);

	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);

	while(1)
	{
	}

	return 0;
}

void SystemClock_config_HSI(uint8_t Clock_freq)
{
	RCC_OscInitTypeDef Osc_Init;
	RCC_ClkInitTypeDef Clk_Init;
	uint32_t F_latency = 0;


	Osc_Init.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	Osc_Init.HSIState = RCC_HSI_ON;
	Osc_Init.HSICalibrationValue = 16;
	Osc_Init.PLL.PLLState = RCC_PLL_ON;
	Osc_Init.PLL.PLLSource = RCC_PLLSOURCE_HSI;

	Clk_Init.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	Clk_Init.SYSCLKSource= RCC_SYSCLKSOURCE_PLLCLK;
	Clk_Init.APB2CLKDivider = RCC_HCLK_DIV2;
	switch(Clock_freq)
	{
		case SYS_CLOCK_FREQ_50MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 100;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV2;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_1WS;
			break;
		case SYS_CLOCK_FREQ_84MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 168;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;


			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_2WS;
			break;
		case SYS_CLOCK_FREQ_120MHZ:
			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 240;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV4;

			F_latency = FLASH_ACR_LATENCY_3WS;
			break;
		case SYS_CLOCK_FREQ_168MHZ:

			Osc_Init.PLL.PLLM = 8;
			Osc_Init.PLL.PLLN = 240;
			Osc_Init.PLL.PLLP = 2;
			Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV1;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV4;

			F_latency = FLASH_ACR_LATENCY_3WS;

			Osc_Init.PLL.PLLM = 4;
			Osc_Init.PLL.PLLN = 168;
			Osc_Init.PLL.PLLP = 2;
			//Osc_Init.PLL.PLLQ = 2;

			Clk_Init.AHBCLKDivider = RCC_SYSCLK_DIV2;
			Clk_Init.APB1CLKDivider = RCC_HCLK_DIV2;

			F_latency = FLASH_ACR_LATENCY_3WS;

		break;
		default:
			return;
	}

	if( HAL_RCC_OscConfig(&Osc_Init) != HAL_OK)
	{
		Error_handler();
	}

	if( HAL_RCC_ClockConfig(&Clk_Init, F_latency) != HAL_OK)
	{
		Error_handler();
	}

	//systick configuration

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
}

void GPIO_LED_Init(void)
{
	GPIO_InitTypeDef ledgpio;
	__HAL_RCC_GPIOD_CLK_ENABLE();

	//PD14
	ledgpio.Mode = GPIO_MODE_OUTPUT_PP;
	ledgpio.Speed = GPIO_SPEED_FAST;
	ledgpio.Pin = GPIO_PIN_14;
	ledgpio.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOD, &ledgpio);
	ledgpio.Pin = GPIO_PIN_15;
	HAL_GPIO_Init(GPIOD, &ledgpio);
	ledgpio.Pin = GPIO_PIN_12;
	HAL_GPIO_Init(GPIOD, &ledgpio);
	ledgpio.Pin = GPIO_PIN_13;
	HAL_GPIO_Init(GPIOD, &ledgpio);
}

void TIMER6_Init(void)
{
	htimer6.Instance = TIM6;
	htimer6.Init.Prescaler = 24;
	htimer6.Init.Period = 64000-1;
	if(HAL_TIM_Base_Init(&htimer6) != HAL_OK)
	{
		Error_handler();
	}
}

void Error_handler(void)
{
	while(1);
}
