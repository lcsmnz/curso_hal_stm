#ifndef MAIN_H_
#define MAIN_H_

#include "stm32f4xx_hal.h"

#define SYS_CLOCK_FREQ_50MHZ 50
#define SYS_CLOCK_FREQ_84MHZ 84
#define SYS_CLOCK_FREQ_120MHZ 120
#define SYS_CLOCK_FREQ_168MHZ 168

#endif
